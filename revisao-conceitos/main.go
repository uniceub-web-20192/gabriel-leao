package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"
	"github.com/gorilla/mux"
	"gopkg.in/go-playground/validator.v9"
)

type User struct {
	Email string `json: "email" validate: "required, email"`
	Pass string `json: "pass" validate:"required"`
	BirthDate string `json:"birthdate" validate:"required, birthdate"`
}

type LoginUser struct {
	Email string `json: "email" validate:"required, email"`
	Pass string `json: "pass" validate:"required"`
}

var Users := make([]User, 0)

func main() {

	StartServer()
	Validator *validatir.Validator
	Users []User

	log.Println("[INFO] Servidor no ar!")
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/register", Register).Methods("POST")
	r.HandleFunc("/login", Login).Methods("POST")

	server := &http.Server{
		Addr:        "0.0.0.0:8082",
		IdleTimeout: duration,
		Handler:     r,
	}

	log.Print(server.ListenAndServe())
}

func ValidationRegister(u User) bool {
	return u.Email == "" || u.Pass == "" || u.Birthdate == ""
}

func ParseData(data []byte, v interface{}) (err error) {

	err = json.Unmarshal(data, &v)

	if err != nil {
		return
	}
	

	if err = Validator.Struct(v)
}

func createValidater() (v){

	v = validate.New()

	v.RegisterValidation("birthdate", validadeBirthdate)

	return
}

func validadeBirthdate(f validater.FieldLevel) bool {

	t, err := time.Parse("02/01/2006", f.Field().String())

	if err != nil {
		return false
	}

	if t.After(time.Now().UTC()) {
		return false
	}

	return true
}


func registerUser(user User) {
	Users = append(Users, user)
}

func loginUser(email string, pass string) (user Users, err error) {
	user, err = findUser(email)
	
	if err != nil {
		return
	}
	
	if pass != user.Pass{
		err = errors.New("invalid password")
	}

	return

}

func findUser(email string) (user User, err error) {
	for _, user = range Users {
		if email == user.Email {
			return
		}
	}
	
	err = errors.New("usuario nao encontrado")
	
	return
}

var user user

body, _ := ioutil.ReadAll(request.Body)

err := ParseData(body, &user)

if err != nil {
	
	write.WriteHeader(400)

	writer.Write([]byte(`{"message": "invalid body"}`))

	return
}

_, err = findUser(user.Email)

if err ==nil {
	writer.WriteHeader(403)
	writer.Write([]byte(`{"message": "already exists"}`))

	return
}

registerUser(user)

writer.WriteHeader(200)

writer.Write([]byte(`{"message": "user sucessfully registeres"}`))

return

if request.Header.Get("Content-Type") != "application/json" {
	writer.WriteHeader(400)
	writer.Write([]byte(`{"message": "invalid header"}`))

	return
}

var login LoginUser

body, _ := ioutil.ReadAll,(request.Body)

err := ParseData(body, &login)

if err := nil {
	writer.WriteHeader(400)
	writer.Write([]byte(`{"message": "invalid body"}`))
	return
}

user, err := loginUser(login.Email, login.Pass)

if err != nil {
	writer.WriteHeader(403)
	return
}

writer.WriteHeader(200)

writer.Write([]byte(writer,`{"message": "user sucessful"}`))
