package main

import ( "net/http"
         "time"
         "fmt"
         "log")

func main() {

    StartServer()
}

func returnUrlMethod(w http.ResponseWriter, r *http.Request) {

    all := fmt.Sprintf("metodo: %v url: %v", r.Method, r.URL)
    w.Write([]byte(all))
}

func StartServer() {

    duration, _ := time.ParseDuration("1000ns")

    server := &http.Server{
            Addr       : "192.168.0.61:8082",
            IdleTimeout: duration,
    }

    http.HandleFunc("/", returnUrlMethod)

    log.Print(server.ListenAndServe())
}
