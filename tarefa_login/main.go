package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"
	"github.com/gorilla/mux"
)

type User struct {
	Email string `json:"email"`
	Pass string `json:"pass"`
	BirthDate string `json:"birthdate"`
}

var users = make([]User, 0)

func main() {

	StartServer()

	log.Println("[INFO] Servidor no ar!")
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/register", Register).Methods("POST")
	r.HandleFunc("/login", Login).Methods("POST")

	server := &http.Server{
		Addr:        "0.0.0.0:8082",
		IdleTimeout: duration,
		Handler:     r,
	}

	log.Print(server.ListenAndServe())
}

func UserValidator(u User) bool {
	return u.Email == "" || u.Pass == "" || u.Birthdate == ""
}

func LoginValidator(u User) bool {
	return u.Email == "" || u.Pass == ""
}

func Register(res http.ResponseWriter, req *http.Request) {

	var u User

	body, _ := ioutil.ReadAll(req.Body)

	json.Unmarshal(body, &u)

	if UserValidator(u) {
		res.WriteHeader(400)
		res.Write([]byte("{\"message\": \"invalid body\"}"))
		return
	}

	if request.Header.Get("Content-Type") != "application/json" {
		writer.WriteHeader(400)
		writer.Write([]byte(`{"message": "invalid header"}`))
	
		return
	}

	users = append(users, u)

	res.WriteHeader(200)
	res.Write([]byte("{\"message\": \"user successfully registered\"}"))
}

func invalidPassword(u User) bool {
	var user User

	for _, user = range users {
		if u.Email == user.Email {
			fmt.Println("u.Email :", u.Email, "user.Email", user.Email, "u.Pass: ", u.Pass, "user.Pass", user.Pass)
			return !(u.Pass == user.Pass)
		}
	}

	return true
}

func Login(res http.ResponseWriter, req *http.Request) {

	var u User

	body, _ := ioutil.ReadAll(req.Body)

	json.Unmarshal(body, &u)

	if LoginValidator(u) {
		res.WriteHeader(400)
		res.Write([]byte("{\"message\": \"invalid body\"}"))
		return
	}

	if request.Header.Get("Content-Type") != "application/json" {
		writer.WriteHeader(400)
		writer.Write([]byte(`{"message": "invalid header"}`))
	
		return
	}

	if invalidPassword(u) {
		res.WriteHeader(403)
		return
	}

	res.WriteHeader(200)
	res.Write([]byte("{\"message\": \"user successfully login\"}"))
}

